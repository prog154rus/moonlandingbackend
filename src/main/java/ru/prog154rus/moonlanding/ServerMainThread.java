package ru.prog154rus.moonlanding;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import ru.prog154rus.moonlanding.worker.MoveServerHandler;

public class ServerMainThread {

    private int port;

    public ServerMainThread(int port) {
        this.port = port;
    }

    public void run() throws InterruptedException {
        EventLoopGroup recieverGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(recieverGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel socketChannel) throws Exception {
                            socketChannel.pipeline().addLast(new MoveServerHandler());
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
            ;

            ChannelFuture f = bootstrap.bind(port).sync();

            f.channel().closeFuture().sync();

        }
        finally {
            workerGroup.shutdownGracefully();
            recieverGroup.shutdownGracefully();
        }

    }
}
