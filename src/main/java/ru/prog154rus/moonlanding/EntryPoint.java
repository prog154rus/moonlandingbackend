package ru.prog154rus.moonlanding;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

public class EntryPoint {
    public static void main(String[] args) throws Exception {
        ServerMainThread mainThread = new ServerMainThread(8080);
        mainThread.run();
    }
}
