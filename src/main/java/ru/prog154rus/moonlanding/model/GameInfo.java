package ru.prog154rus.moonlanding.model;

public class GameInfo {

    private final String guid;
    private final float height;
    private final float currentSpeed;
    private final float fuelLeft;

    public GameInfo(
            String guid,
            float height,
            float currentSpeed,
            float fuelLeft) {
        this.guid = guid;
        this.height = height;
        this.currentSpeed = currentSpeed;
        this.fuelLeft = fuelLeft;
    }

    public String getGuid() {
        return guid;
    }

    public float getHeight() {
        return height;
    }

    public float getCurrentSpeed() {
        return currentSpeed;
    }

    public float getFuelLeft() {
        return fuelLeft;
    }
}
